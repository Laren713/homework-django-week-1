from django.contrib.auth.models import User
from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from accounts.forms import ReviewForm
from accounts.models import Review
from .forms import AuthorForm, BookForm
from .models import Authors, Books
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView


class WellcomeView(TemplateView):
    template_name = 'new_bookstore/index.html'


def show_all_books(request):
    books = Books.objects.all().order_by('-id')
    context = {'books': books}
    return render(request, 'new_bookstore/book_list.html', context=context)


class BookDescriptionView(DetailView):
    model = Books
    template_name = 'new_bookstore/book_description.html'


def book_detail(request, id):
    form_r = ReviewForm(request.POST or None)
    book = get_object_or_404(Books, id=id)
    books = Books.objects.filter(id=id)
    if request.method == 'POST':
        if request.user.is_authenticated:
            if form_r.is_valid():
                temp = form_r.save(commit=False)
                temp.reviewer = User.objects.get(id=request.user.id)
                temp.book = book
                temp = Books.objects.get(id=id)
                form_r.save()
                temp.save()
                messages.success(request, 'Review Added Successfully')
    context = {
        "book": book,
        "books": books,
        "form_r": ReviewForm,
    }
    return render(request, "new_bookstore/book_detail.html", context)


def show_book_reviews(request, index):
    reviews = Review.objects.filter(book_id=index).order_by('-created_date')
    context = context = {'reviews': reviews}
    return render(request, 'new_bookstore/book_review.html', context=context)


def show_all_authors(request):
    authors = Authors.objects.all().order_by('-id')
    context = {'authors': authors}
    return render(request, 'new_bookstore/author_list.html', context=context)


class AuthorDetailView(DetailView):
    model = Authors
    template_name = 'new_bookstore/author_detail.html'


def show_authors_books(request, index):
    books = Books.objects.filter(author_id=index)
    context = context = {'books': books}
    return render(request, 'new_bookstore/book_list.html', context=context)


def search_book(request):
    books = Books.objects.all()
    if request.method == 'GET' and "book" in request.GET:
        book = request.GET['book']
        books = books.filter(title=book)
    context = {'books': books}
    return render(request, 'new_bookstore/book_list.html', context=context)


def add_new_book(request):
    books = Books.objects.all()
    if request.method == 'POST':
        form_b = BookForm(request.POST)
        if form_b.is_valid():
            form_b.save()
            return redirect('book_list')
    context = {'books': books, 'book_form': BookForm}
    return render(request, 'new_bookstore/add_new_book.html', context=context)


def add_new_author(request):
    authors = Authors.objects.all()
    if request.method == 'POST':
        form_a = AuthorForm(request.POST)
        if form_a.is_valid():
            form_a.save()
            return redirect('author_list')
    context = {'authors': authors, 'author_form': AuthorForm}
    return render(request, 'new_bookstore/add_new_author.html', context=context)