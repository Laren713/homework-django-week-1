from django.db import models
from django.shortcuts import get_object_or_404

books = [{'id': 1,
          'title': 'Fluent Python',
          'released_year': 2015,
          'description': 'Python’s simplicity lets you become productive quickly, but this often means you aren’t using everything it has to offer. With this hands-on guide, you’ll learn how to write effective, idiomatic Python code by leveraging its best—and possibly most neglected—features. Author Luciano Ramalho takes you through Python’s core language features and libraries, and shows you how to make your code shorter, faster, and more readable at the same time.',
          'author_id': 1},
         {'id': 2,
          'title': 'The Python workbook a brief',
          'released_year': 2019,
          'description': 'This book contains 186 exercises that span a variety of academic disciplines and everyday situations. They can be solved using only the material covered in most introductory Python programming courses. Each exercise that you complete will strengthen your understanding of the Python programming language and enhance your ability to tackle subsequent programming challenges.',
          'author_id': 2},
         {'id': 3,
          'title': 'Easy Python. Modern style of programming',
          'released_year': 2016,
          'description': 'The book is a leisurely introduction that will gradually guide you from the basics to many more profound topics. I used a mixture of textbook and cookbook styles to explain new terms and ideas in turn. The code written in Python is included even in the very first chapters.',
          'author_id': 3},
         {'id': 4,
          'title': 'A byte of Python',
          'released_year': 2020,
          'description': 'This book serves as a guide or tutorial for learning the programming language Python. It is aimed primarily at beginners. However, it will be useful and experienced programmers.',
          'author_id': 4},
         {'id': 5,
          'title': 'Python by example',
          'released_year': 2019,
          'description': 'This book is a hands-on approach to learning programming. After minimal reading you are set a number of challenges to create the programs.',
          'author_id': 5},
         {'id': 6,
          'title': 'Clean Python',
          'released_year': 2019,
          'description': 'The main goal of this book is to provide tips to Python developers of various levels so they can write better Python software and programs. This book gives you various techniques irrespective of the field you use Python in. This book covers all levels of Python, from basic to advanced, and shows you how to make your code more Pythonic.',
          'author_id': 6},
         {'id': 7,
          'title': 'Python. Pocket reference',
          'released_year': 2015,
          'description': 'Updated for both Python 3.4 and 2.7, this convenient pocket guide is the perfect on-the-job quick reference. You ll find concise, need-to-know information on Python types and statements, special method names, built-in functions and exceptions, commonly used standard library modules, and other prominent Python tools. The handy index lets you pinpoint exactly what you need.',
          'author_id': 7},
         {'id': 8,
          'title': 'Python. Pocket reference. Second Edition',
          'released_year': 2016,
          'description': 'You ll find concise, need-to-know information on Python types and statements, special method names, built-in functions and exceptions, commonly used standard library modules, and other prominent Python tools. The handy index lets you pinpoint exactly what you need.',
          'author_id': 7}

]
authors = [{'id': 1,
            'first_name': 'Luciano',
            'last_name': 'Ramalho',
            'age': 51 },
           {'id': 2,
            'first_name': 'Ben',
            'last_name': 'Stephenson',
            'age': 35},
           {'id': 3,
            'first_name': 'Bill',
            'last_name': 'Lubanovic',
            'age': 60},
           {'id': 4,
            'first_name': 'Swaroop',
            'last_name': 'Chitlur',
            'age': 48},
           {'id': 5,
            'first_name': 'Nichola',
            'last_name': 'Lacey',
            'age': 55},
           {'id': 6,
            'first_name': 'Sunil',
            'last_name': 'Kapil',
            'age': 37},
           {'id': 7,
            'first_name': 'Mark',
            'last_name': 'Lutz',
            'age': 81}
]


class Authors(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=30)
    age = models.IntegerField(default=0)

    @staticmethod
    def add_author():
        for i in authors:
            author = Authors(first_name=i['first_name'], last_name=i['last_name'], age=i['age'])
            author.save()

    def __str__(self):
        return f"{self.pk}: {self.first_name} {self.last_name}"

    class Meta:
        verbose_name = 'Author'
        verbose_name_plural = 'Authors'
        ordering = ['first_name','last_name']


class Books(models.Model):
    title = models.CharField(max_length=100)
    released_year = models.IntegerField(default=0)
    description = models.TextField()
    author = models.ForeignKey(Authors, on_delete=models.CASCADE)

    @staticmethod
    def add_book():
        for i in books:
            author = get_object_or_404(Authors, pk=i['author_id'])
            book = Books(title=i['title'], released_year=i['released_year'], description=i['description'], author=author)
            book.save()

    def __str__(self):
        return f"{self.pk} {self.title}"

    class Meta:
        verbose_name = 'Book'
        verbose_name_plural = 'Books'
        ordering = ['title', 'released_year']

