from django.urls import path
from new_bookstore.views import *

urlpatterns = [
    path('', WellcomeView.as_view(), name='index'),
    path('books/search/', search_book, name='search_book'),
    path('books/', show_all_books, name='book_list'),
    path('book/des/<int:pk>/', BookDescriptionView.as_view(), name='book_description'),
    path('book/rev/<int:index>/', show_book_reviews, name='book_review'),
    path('book/<int:id>/', book_detail, name='book_detail'),
    path('authors/', show_all_authors, name='author_list'),
    path('author/<int:pk>/', AuthorDetailView.as_view(), name='author_detail'),
    path('author/book/<int:index>/', show_authors_books, name='book_list'),
    path('add-book/', add_new_book, name='add_new_book'),
    path('add-author/', add_new_author, name='add_new_author')

]