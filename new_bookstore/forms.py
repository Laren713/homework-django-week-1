from django import forms
from new_bookstore.models import Books, Authors


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Authors
        fields = ['first_name', 'last_name', 'age']


class BookForm(forms.ModelForm):
    class Meta:
        model = Books
        fields = ['title', 'released_year', 'description', 'author']
