from django.contrib import admin
from .models import Authors, Books


class AuthorAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Authors._meta.fields]
    search_fields = [field.name for field in Authors._meta.fields]
    list_filter = ['last_name']
    list_editable = ('age',)
    empty_value_display = '-empty-'

    class Meta:
        model = Authors


class BookAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Books._meta.fields]
    search_fields = [field.name for field in Books._meta.fields]
    list_filter = ['title', 'released_year', 'author']
    list_editable = ('description',)

    class Meta:
        model = Books


admin.site.register(Authors, AuthorAdmin)
admin.site.register(Books, BookAdmin)