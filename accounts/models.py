from django.contrib.auth import get_user_model
from django.db import models
from new_bookstore.models import Books


class Review(models.Model):
	reviewer = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
	book = models.ForeignKey(Books, on_delete=models.CASCADE)
	review_star = models.IntegerField()
	review_text = models.TextField()
	created_date = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = 'Reviews'
		verbose_name_plural = 'Reviews'
		ordering = ['created_date']