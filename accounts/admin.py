from django.contrib import admin
from .models import Review


class ReviewAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Review._meta.fields]
    search_fields = [field.name for field in Review._meta.fields]
    list_filter = ['review_star', 'book', 'reviewer', 'created_date']
    list_editable = ('review_star', 'review_text')
    empty_value_display = '-empty-'

    class Meta:
        model = Review


admin.site.register(Review, ReviewAdmin)
